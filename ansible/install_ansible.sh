#!/bin/bash

#########################################
# 	Install Ansible
#########################################
# Maintainer:	dont panic it-services
# Author: 	Marco Huber mh@panic.at
# Date: 	25.06.2018
#########################################

DATE=$(date "+%d %b %H:%M")
# Settings
ANSIBLE=/panic/logs/install_anisble.log

echo "$DATE Installation Ansible" >> $ANSIBLE


if [ -e /panic/ctrl/install_ansible ] && [ ! -e /panic/ctrl/ansible_is_installed ]; then
	# Ansible Installation
		# Add apt source list
		cp /panic/setup/panic_apps/ansible/ansible.list /etc/apt/sources.list.d/
		echo "add ansible.list to source.list.d" >> $ANSIBLE
		# Add Key
		apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
		echo "add apt key for ansible" >> $ANSIBLE
		# Update and Install
		apt-get update && apt-get install ansible -y
		echo "install ansible" >> $ANSIBLE

		# Safe Installation
		touch /panic/ctrl/ansible_is_installed
		rm /panic/ctrl/install_ansible
else
	echo "create /panic/ctrl/install_ansible or is allready installed"
fi

exit 0
