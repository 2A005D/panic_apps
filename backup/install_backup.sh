#!/bin/bash

#########################################
# 	Install Backup Scripts
#########################################
# Maintainer:	dont panic it-services
# Author: 	Marco Huber mh@panic.at
# Date: 	04.07.2018
#########################################

# Prepare
apt-get update && apt-get install -y tclsh

# Add Database Backup Directory
  if [ ! -d /var/backups/databases ]; then
    mkdir /var/backups/databases
  fi
# Add Database Backup Script
  cp /panic/setup/panic_apps/backup/databaseBackups.sh /usr/local/sbin/

# Add Backup Script for /var/www
  cp /panic/setup/panic_apps/backup/rsync/rsync_backup.sh /usr/local/sbin/
  if [ ! -d /etc/rsync_backup ]; then
    mkdir /etc/rsync_backup
  fi
# Add Backup Config for Rsync
  cp /panic/setup/panic_apps/backup/rsync/rsync_backup.cfg /etc/rsync_backup/

# Add Backup-Path for Rsync
  if [ ! -d /var/backups/rsync_backups/var/www ]; then
    mkdir -p /var/backups/rsync_backups/var/www
  fi

# Cronjob
  echo "17 2    * * *   root /usr/local/sbin/databaseBackups.sh > /var/log/backup.log 2>&1" >> /etc/crontab
  echo "27 2    * * *   root /usr/local/sbin/rsync_backup.sh > /var/log/backup.log 2>&1" >> /etc/crontab



# Change Rights on /usr/local/sbin
  chown root:root /usr/local/sbin/*
  chmod 700 /usr/local/sbin/*
