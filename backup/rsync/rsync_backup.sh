#!/usr/bin/tclsh

#########################################
# Rsync Directory Backup Script
# config = /etc/rsync_backup/rsync_backup.cfg
#########################################
# Maintainer:	dont panic it-services
# Author: 	Christian Wally cw@panic.at
# Date: 	25.06.2018
#########################################

proc rotateFile {filename {rotations {}}} {
    incr rotations -1
    # Delete the oldest backup file if exists
    if {[file exists $filename.$rotations]} {
        # "Deleting $filename.$rotations"
        file delete -force $filename.$rotations
    }
    incr rotations -1

    # rotate the remaining versions
    for {set count $rotations} {$count > 0} {incr count -1} {
        set previous [expr $count + 1]

        if {[file exists $filename.$count]} {
            #  "Renaming $filename.$count to $filename.$previous"
            file rename $filename.$count $filename.$previous
        }
    }

    if {[file exists $filename]} {
        set previous [expr $count + 1]
        #  "Renaming $filename to $filename.$previous"
        file rename $filename $filename.$previous
    }
}


proc readConfig {filename} {

    if {[file exists $filename]} {
        global cfg
        # Read the file in
        set f [open $filename]
        set contents [read $f]
        close $f

        # Parse the file's contents
        foreach line [split $contents "\n"] {
            set line [string trim $line]
            # Skip comments
            if {[string match "#*" $line] || [string match ";*" $line]} continue
            # Skip blanks
            if {$line eq ""} continue

            if {[regexp {^\w+$} $line]} {
                # Boolean case
                set cfg([string tolower $line]) true
            } elseif {[regexp {^(\w+)\s+([^,]+)$} $line -> var value]} {
                # Simple value case
                set cfg([string tolower $var]) $value
            } elseif {[regexp {^(\w+)\s+(.+)$} $line -> var listValue]} {
                # List value case
                set cfg([string tolower $var]) {}
                foreach value [split $listValue ","] {
                    lappend cfg([string tolower $var]) [string trim $value]
                }
            } else {
                puts "malformatted config file: $filename"
                exit 1
            }
        }

    } else {
        puts "config file does not exit: $filename"
        exit 1
    }
}



if {[llength $argv] == 1} {
    set config_filename  [lindex $argv 0]
} else {
    set config_filename  "/etc/rsync_backup/rsync_backup.cfg"
}

readConfig $config_filename
rotateFile "$cfg(log_path)backup.log" $cfg(rotations)

foreach dir [split $cfg(backup_dirs)] {
	rotateFile "$cfg(local_path)/$dir" $cfg(rotations)

  if {[file exists $cfg(local_path)$dir.1]} {
    #  Using rotated Backup as hard link
    exec $cfg(rsync_bin) -v -a --link-dest=$cfg(local_path)$dir.1 --delete $dir/ $cfg(local_path)$dir  >> $cfg(log_path)backup.log
  } else {
    # Making the first Backup
    exec $cfg(rsync_bin) -v -a $dir/ $cfg(local_path)$dir  >> $cfg(log_path)backup.log
  }


}


# exec $cfg(cat_bin) $cfg(log_path)backup.log | $cfg(mailx_bin) -a "From: MSV Backup Robot <$cfg(sender_email)>"  -s "Backup Log" $cfg(rcpt_email)
