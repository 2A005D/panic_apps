#!/bin/bash

#########################################
# 	Install idoit
#########################################
# Maintainer:	dont panic it-services
# Author: 	Marco Huber mh@panic.at
# Date: 	25.06.2018
#########################################

DATE=$(date "+%d %b %H:%M")
# Settings
IDOIT=/panic/logs/install_idoit.log
APACHE=/panic/logs/install_apache2.log
MARIADB=/panic/logs/install_mariadb.log
PHP=/panic/logs/install_php.log

echo "$DATE Installation IDOIT" >> $IDOIT
echo "$DATE Installation IDOIT" >> $APACHE
echo "$DATE Installation IDOIT" >> $MARIADB
echo "$DATE Installation IDOIT" >> $PHP


if [ -e /panic/ctrl/install_idoit ] && [ ! -e /panic/ctrl/idoit_is_installed ]; then
	# Idoit Installation
		# Prepare
		apt-get install -y unzip sudo moreutils
		
		# Apache
		# add allowoverride or vhost.conf
		echo "Add Apache Vhost idoit.conf" >> $APACHE
		cp /panic/setup/panic_apps/idoit/apache.conf /etc/apache2/sites-available/i-doit.conf
		a2ensite i-doit.conf

		# PHP
		cp /panic/setup/panic_apps/idoit/i-doit.ini /etc/php/7.0/mods-available/
		echo "add i-doit.ini to php mods (available)" >> $PHP
		phpenmod mcrypt
		phpenmod memcached
		phpenmod i-doit
		echo "phpenmod i-doit,mcrypt and memcached" >> $PHP

		# MariaDB
		systemctl stop mysql
		echo "Stop Mariadb for ib_logfile configuration" >> $MARIADB
		mv /var/lib/mysql/ib_logfile[01] /tmp
		echo "backup /var/lib/mysql/ib_logile[01] on /tmp" >> $MARIADB
		cp /panic/setup/panic_apps/idoit/99-i-doit.cnf /etc/mysql/mariadb.conf.d/
		echo "Install 99-i-doit.cnf in /etc/mysql/mariadb.conf.d/" >> $MARIADB


		# WebRoot
		mkdir /var/www/html/i-doit/

		# Services
		systemctl start mysql
		systemctl restart apache2

		# Idoit
		echo "download and unarchive idoit.zip to /var/www/html/i-doit/"
		echo "cd /var/www/html/i-doit/"
		echo "chown www-data:www-data -R ."
		echo "find . -type d -name \* -exec chmod 775 {} \;"
		echo "find . -type f -exec chmod 664 {} \;"
		echo "chmod 774 controller *.sh setup/*.sh"


		# Safe Installation
		touch /panic/ctrl/idoit_is_installed
		rm /panic/ctrl/install_idoit
else
	echo "create /panic/ctrl/install_idoit or is allready installed"
fi

exit 0
