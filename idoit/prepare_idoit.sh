#!/bin/bash

#########################################
# 	Prepare idoit
#########################################
# Maintainer:	dont panic it-services
# Author: 	Marco Huber mh@panic.at
# Date: 	18.07.2018
#########################################

# Version Aktueller Container
IMAGEFILE="/panic/ctrl/image"
CONTAINERIMAGE=$( cat $IMAGEFILE )

if [ $CONTAINERIMAGE = "panic_debian-stretch-systemd-lamp" ]; then
  echo "finishd"
  echo "do"
  echo "sh /panic/setup/panic_apps/php/install_php.sh remove yes"
  echo "sh /panic/setup/panic_apps/php/install_php.sh install default"
  echo "sh /panic/setup/panic_apps/idoit/install_idoit.sh"
else
  echo "do"
  echo "if php is installed do sh /panic/setup/panic_apps/php/install_php.sh remove yes"
  echo "sh /panic/setup/panic_apps/php/install_php.sh install default"
  echo "sh /panic/setup/panic_apps/mariadb/install_mariadb.sh"
  echo "sh /panic/setup/panic_apps/idoit/install_idoit.sh"
fi
