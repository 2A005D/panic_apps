#!/bin/bash

#########################################
# 	Install Mariadb
#########################################
# Maintainer:	dont panic it-services
# Author: 	Marco Huber mh@panic.at
# Date: 	18.07.2018
#########################################

DATE=$(date "+%d %b %H:%M")
DBROOTPASS=${SQLROOT_PASSWORD:-$(pwgen -s -1 16)}

MARIADB=/panic/logs/install_mariadb.log
MARIADBPASSWORD=/var/lib/mysql/panic.cnf

# Install MariaDB
apt-get install -y mariadb-server mariadb-client

# Setup MariaDB
  echo "Start Mariadb for first setting up" >> $MARIADB
  systemctl start mariadb
  sleep 2
  cp -r /panic/setup/panic_apps/mariadb/90-panic.cnf /etc/mysql/mariadb.conf.d/
  chmod 640 /etc/mysql/mariadb.conf.d/90-panic.cnf
  sleep 1
  mysql -u root -e"UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE User = 'root'; FLUSH PRIVILEGES; SET GLOBAL innodb_fast_shutdown = 0"
  echo "Set mysql_nativ_password on root" >> $MARIADB
  mysqladmin -u root password $DBROOTPASS
  echo "[mysql]" >> $MARIADBPASSWORD && echo "$DBROOTPASS" >> $MARIADBPASSWORD
  chmod 400 $MARIADBPASSWORD
  sed -i "s/password=/password=$DBROOTPASS/g" /etc/mysql/mariadb.conf.d/90-panic.cnf
  systemctl stop mariadb
  # Add Database Backup Directory
  if [ ! -d /var/backups/databases ]; then
    mkdir /var/backups/databases
  fi
  cp /panic/setup/panic_apps/backup/databaseBackups.sh /usr/local/sbin/
  # Change Rights on /usr/local/sbin
    chown root:root /usr/local/sbin/databaseBackups.sh
    chmod 700 /usr/local/sbin/databaseBackups.sh
  echo "Add Backup Script to /usr/local/sbin" >> $MARIADB
