#!/bin/bash

ARG1=$1
ARG2=$2

DATE=$(date "+%d %b %H:%M")
PHP=/panic/logs/install_php.log

usage(){
  cat <<EOF
Usage:
$(basename $0) install version (PHP Installation)
$(basename $0) remove yes (PHP Deinstallation default php7.2)

Aufruf: $(basename $0) [OPTIONEN] [VALUE]
        -h, --help              Print this Message
        install,                Installation VALUE=PHP Version außer default ist List von https://packages.sury.org/php/
        remove,                 Deinstallation VALUE = yes
        update,                 update-alternatives --config php

Verfügbare Versionen
  7.0     ->  source list https://packages.sury.org/php/
  7.1     ->  source list https://packages.sury.org/php/
  7.2     ->  source list https://packages.sury.org/php/
  default ->  entfernt gegebenfalls source list https://packages.sury.org/php/  und installiert php os default php
EOF
}

add_list() {
  if [ -e /etc/apt/sources.list.d/php.list ]; then
    rm -r /etc/apt/sources.list.d/php.list
  fi
  echo "deb https://packages.sury.org/php/ stretch main" > /etc/apt/sources.list.d/php.list
  # Add Key
  wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
  # Update it
  apt-get update
}


# PHP 7.0
install_php70() {
  apt-get install -y libapache2-mod-php7.0 php7.0 php7.0-bcmath php7.0-cli php7.0-common \
  php7.0-curl php7.0-gd php7.0-imagick php7.0-json php7.0-ldap php7.0-memcached php7.0-mbstring \
  php7.0-dom php7.0-mysql php7.0-soap php7.0-xml php7.0-zip php7.0-fpm php7.0-snmp memcached mcrypt
  sed -i "s/\;date\.timezone\ \=/date\.timezone\ \=\ UTC/" /etc/php/7.0/apache2/php.ini
  echo "set timezone = UTC" >> $PHP
}

install_php71() {
  # PHP 7.1
  apt-get install -y libapache2-mod-php7.1 php7.1 php7.1-bcmath php7.1-cli php7.1-common \
  php7.1-curl php7.1-gd php7.1-imagick php7.1-json php7.1-ldap php7.1-memcached php7.1-mbstring \
  php7.1-dom php7.1-mysql php7.1-soap php7.1-xml php7.1-zip php7.1-fpm php7.1-snmp memcached mcrypt
  sed -i "s/\;date\.timezone\ \=/date\.timezone\ \=\ UTC/" /etc/php/7.1/apache2/php.ini
  echo "set timezone = UTC" >> $PHP
}

install_php72() {
  # PHP 7.2 -> if lamp image php7.2 is allready exist
  apt-get install -y libapache2-mod-php7.2 php7.2 php7.2-bcmath php7.2-cli php7.2-common \
  php7.2-curl php7.2-gd php7.2-imagick php7.2-json php7.2-ldap php7.2-memcached php7.2-mbstring \
  php7.2-dom php7.2-mysql php7.2-soap php7.2-xml php7.2-zip php7.2-fpm php7.2-snmp memcached mcrypt
  sed -i "s/\;date\.timezone\ \=/date\.timezone\ \=\ UTC/" /etc/php/7.2/apache2/php.ini
  echo "set timezone = UTC" >> $PHP
}

install_phpdefault() {
  apt-get install -y libapache2-mod-php php php-bcmath php-cli php-common \
  php-curl php-gd php-imagick php-json php-ldap php-mbstring php-mcrypt php-memcached php-mysql php-pgsql \
  php-soap php-xml php-zip memcached
  #unzip sudo moreutils
  sed -i "s/\;date\.timezone\ \=/date\.timezone\ \=\ UTC/" /etc/php/7.0/apache2/php.ini
  echo "set timezone = UTC" >> $PHP
}

php_remove() {
  if [ -e /etc/apt/sources.list.d/php.list ]; then
    rm -r -f /etc/apt/sources.list.d/php.list
  fi
  apt-get update
  apt-get remove --purge php7* -y
}

case "$ARG1" in
        -h|--help)
                usage
        ;;
        install)
                case "$ARG2" in
                  7.0)
                      echo "Installation PHP Version 7.0"
                      add_list
                      install_php70
                      echo "$DATE Installation PHP Version 7.0" >> $PHP
                  ;;
                  7.1)
                      echo "Installation PHP Version 7.1"
                      add_list
                      install_php71
                      echo "$DATE Installation PHP Version 7.1" >> $PHP
                  ;;
                  7.2)
                      echo "Installation PHP Version 7.2"
                      add_list
                      install_php72
                      echo "$DATE Installation PHP Version 7.2" >> $PHP
                  ;;
                  default)
                      if [ -e /etc/apt/sources.list.d/php.list ]; then
                        echo "Deinstallation PHP Version 7.*"
                        php_remove
                      fi

                      echo "Installation PHP Version os default"
                      install_phpdefault
                      echo "$DATE Installation PHP Version os defult" >> $PHP
                  ;;
                  *)
                    echo "nothing do"
                    usage
                  ;;
                esac
        ;;
        remove)
              case "$ARG2" in
                yes)
                  echo "Deinstallation PHP Version 7.*"
                  php_remove
                  echo "$DATE Deinstallation PHP Version 7.*" >> $PHP
                ;;
                *)
                  echo "nothing do say \"yes\""
                ;;
              esac

        ;;
        update)
          update-alternatives --config php
        ;;
        *)
                usage
        ;;
esac
