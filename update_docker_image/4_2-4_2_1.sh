#!/bin/bash

# Maintainer:	dont panic it-services (mh@panic.at)
# Version:		4.2
# Update:     4.2.1

# Update wenn Version = ist
UPDATEDEFAULT="0.4.2"
UPDATELAMP="1.4.2"
# Neu Version =
NEWDEFAULT="0.4.2.1"
NEWLAMP="1.4.2.1"

# Message
UPDATEMESSAGE="[0-1].4.2.1"

DATE=$(date "+%d %b %H:%M")
LOGFILE=/panic/logs/image_update.log

# Version Aktueller Container
VERSIONFILE="/panic/ctrl/version"
IMAGEFILE="/panic/ctrl/image"
CONTAINERVERSION=$( cat $VERSIONFILE )
CONTAINERIMAGE=$( cat $IMAGEFILE )

# Name der Images
DEFAULTIMAGE="panic_debian-stretch-systemd"
LAMPIMAGE="panic_debian-stretch-systemd-lamp"


### Update Functions ###

# Image: debian-strech-systemd
  update_default_image() {
    # Update Image
    echo $NEWDEFAULT > $VERSIONFILE
    # Nothing to do
  }

# Image: debian-strech-systemd-lamp
  update_lamp_image() {
    # Update Image
    echo $NEWLAMP > $VERSIONFILE
    # Change Rights on mariadb.conf.d/90-panic.cnf
      chmod 640 /etc/mysql/mariadb.conf.d/90-panic.cnf
    # Change Rights on /usr/local/sbin
      chmod 700 /usr/local/sbin/*
  }



# Message and Log
  usage() {
    echo "Update Image:$CONTAINERIMAGE $CONTAINERVERSION to $UPDATEMESSAGE"
    echo "$DATE Update to $UPDATEMESSAGE" >> $LOGFILE
  }

# debian-strech-systemd 0.4.*
  if [ $CONTAINERVERSION = $UPDATEDEFAULT ] && [ $CONTAINERIMAGE = $DEFAULTIMAGE ]; then
    update_default_image
    usage
  fi

# debian-strech-systemd-lamp 1.4.*
  if [ $CONTAINERVERSION = $UPDATELAMP ] && [ $CONTAINERIMAGE = $LAMPIMAGE ]; then
    update_lamp_image
    usage
  fi
