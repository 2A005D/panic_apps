#!/bin/bash

# Maintainer:	dont panic it-services (mh@panic.at)
# Version:		4.2.2
# Update:     4.2.2

# Update wenn Version = ist
UPDATEDEFAULT="0.4.2.1"
UPDATELAMP="1.4.2.1"
# Neu Version =
NEWDEFAULT="0.4.2.2"
NEWLAMP="1.4.2.2"

# Message
UPDATEMESSAGE="[0-1].4.2.2"

DATE=$(date "+%d %b %H:%M")
LOGFILE=/panic/logs/image_update.log

# Version Aktueller Container
VERSIONFILE="/panic/ctrl/version"
IMAGEFILE="/panic/ctrl/image"
CONTAINERVERSION=$( cat $VERSIONFILE )
CONTAINERIMAGE=$( cat $IMAGEFILE )

# Name der Images
DEFAULTIMAGE="panic_debian-stretch-systemd"
LAMPIMAGE="panic_debian-stretch-systemd-lamp"


### Other Functions
updatemessage(){
  cat <<EOF
  /etc/crontab
  17 2    * * *   /usr/local/sbin/databaseBackups.sh
  27 2    * * *   /usr/local/sbin/rsync_backup.sh
  to
  17 2    * * *   root /usr/local/sbin/databaseBackups.sh > /var/log/backup.log 2>&1
  27 2    * * *   root /usr/local/sbin/rsync_backup.sh > /var/log/backup.log 2>&1
EOF
}

### Update Functions ###

# Image: debian-strech-systemd
  update_default_image() {
    # Update Image
    echo $NEWDEFAULT > $VERSIONFILE
    # Nothing to do
  }

# Image: debian-strech-systemd-lamp
  update_lamp_image() {
    # Update Image
    echo $NEWLAMP > $VERSIONFILE
      # Fehler in Crontab
       updatemessage
  }



# Message and Log
  usage() {
    echo "Update Image:$CONTAINERIMAGE $CONTAINERVERSION to $UPDATEMESSAGE"
    echo "$DATE Update to $UPDATEMESSAGE" >> $LOGFILE
  }

# debian-strech-systemd 0.4.*
  if [ $CONTAINERVERSION = $UPDATEDEFAULT ] && [ $CONTAINERIMAGE = $DEFAULTIMAGE ]; then
    update_default_image
    usage
  fi

# debian-strech-systemd-lamp 1.4.*
  if [ $CONTAINERVERSION = $UPDATELAMP ] && [ $CONTAINERIMAGE = $LAMPIMAGE ]; then
    update_lamp_image
    usage
  fi
