#!/bin/bash

#########################################
# dump all databases
#########################################
# Maintainer:	dont panic it-services
# Author: 	Christian Wally cw@panic.at
# Date: 	25.06.2018
#########################################

function rotate {
	file=$1
	echo "rotating $file"
	if [ -f $file.7.gz ]
	then
		rm $file.7.gz
	fi

	for (( counter=6; $counter>=1; counter--))
	do
		counterplus=$(expr $counter + 1)
		if [ -f $file.$counter.gz ]
		then
			mv $file.$counter.gz $file.$counterplus.gz
		fi
	done


        if [ -f $file ]
        then
		echo "compressing $file"
                gzip $file
		mv $file.gz $file.1.gz
        fi
}


for i in `echo 'show databases' | mysql -u root | grep -v information_schema | grep -v performance_schema | grep -v Database `
do
	rotate /var/backups/databases/$i.sql
	echo "Dumping database $i"
	mysqldump -u root --single-transaction $i > /var/backups/databases/$i.sql
done
