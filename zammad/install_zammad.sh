#!/bin/bash

#########################################
# 	Install Zammad
#########################################
# Maintainer:	dont panic it-services
# Author: 	Marco Huber mh@panic.at
# Date: 	03.07.2018
#########################################

DATE=$(date "+%d %b %H:%M")
# Settings
ZAMMAD=/panic/logs/install_zammad.log
DBROOTPASS=${SQLROOT_PASSWORD:-$(pwgen -s -1 16)}
MARIADBPASSWORD=/var/lib/mysql/panic.cnf

echo "$DATE Installation Zammad" >> $ZAMMAD


if [ -e /panic/ctrl/install_zammad ] && [ ! -e /panic/ctrl/zammad_is_installed ]; then
  # MariaDB
    apt-get update && apt-get upgrade -y
    apt-get install -y mariadb-server mariadb-client
    sleep 1
    systemctl start mariadb
    sleep 1

  # Elasticsearch Installation https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html
    # Add apt source list
    # 6.x
    #echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
    #echo "add elasticsearch-6.x.list to source.list.d" >> $ZAMMAD
    #5.x
    echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-5.x.list
    echo "add elasticsearch-5.x.list to source.list.d" >> $ZAMMAD
    # Add Key
    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
    echo "add apt key for elasticsearch" >> $ZAMMAD
    # Update and Install
    apt-get update
    # 6.x
    #apt-get install -y openjdk-9-jre
    #echo "install openjdk-9-jre" >> $ZAMMAD
    # 5.x
    apt-get install -y openjdk-8-jre
    echo "install openjdk-8-jre" >> $ZAMMAD
    apt-get install -y elasticsearch
    echo "install elasticsearch" >> $ZAMMAD
    sleep 2
    # Enable
    systemctl start elasticsearch.service
    systemctl enable elasticsearch.service

	# Zammad Installation https://docs.zammad.org/en/latest/install-debian.html
		# Add apt source list
		cp /panic/setup/panic_apps/zammad/zammad.list /etc/apt/sources.list.d/
		echo "add zammad.list to source.list.d" >> $ZAMMAD
		# Add Key
		wget -qO- https://dl.packager.io/srv/zammad/zammad/key | sudo apt-key add -
		echo "add apt key for zammad" >> $ZAMMAD
		# Update and Install
		apt-get update
    apt-get install zammad -y
		echo "install zammad" >> $ZAMMAD


  # Maria DB / Database
    cp -r /panic/setup/panic_apps/zammad/90-panic.cnf /etc/mysql/mariadb.conf.d/
    chmod 640 /etc/mysql/mariadb.conf.d/90-panic.cnf
    echo "Start Mariadb for first setting up" >> $ZAMMAD
    sleep 1
    mysql -u root -e"UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE User = 'root'; FLUSH PRIVILEGES; SET GLOBAL innodb_fast_shutdown = 0"
    echo "Set mysql_nativ_password on root" >> $ZAMMAD
    mysqladmin -u root password $DBROOTPASS
    echo "[mysql]" >> $MARIADBPASSWORD && echo "$DBROOTPASS" >> $MARIADBPASSWORD
    chmod 400 $MARIADBPASSWORD
    sed -i "s/password=/password=$DBROOTPASS/g" /etc/mysql/mariadb.conf.d/90-panic.cnf
    # Add Database Backup Directory
    if [ ! -d /var/backups/databases ]; then
      mkdir /var/backups/databases
    fi
    cp /panic/setup/panic_apps/zammad/databaseBackups.sh /usr/local/sbin/
    # Change Rights on /usr/local/sbin
    chown root:root /usr/local/sbin/databaseBackups.sh
    chmod 700 /usr/local/sbin/*

    echo "Add Backup Script to /usr/local/sbin" >> $ZAMMAD

  # Cronjob
    echo "17 2    * * *   /usr/local/sbin/databaseBackups.sh" >> /etc/crontab


		# Safe Installation
		touch /panic/ctrl/zammad_is_installed
		rm /panic/ctrl/install_zammad



else
	echo "create /panic/ctrl/install_zammad or is allready installed"
fi

exit 0
